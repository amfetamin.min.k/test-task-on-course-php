import java.io.File

fun main(args: Array<String>) {
    readFileLineByLineUsingForEachLine("../numbers.csv")
}

fun readFileLineByLineUsingForEachLine(fileName: String)
        = File(fileName).forEachLine {
    var count = 0
    for (string in it.split(",")) {
        if (string.toInt() <= 50) println("$string 0")
        else {
            println("$string 1")
            count++
        }
    }
    println("\ncount numbers more 50 = $count")
}