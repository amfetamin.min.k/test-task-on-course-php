fun main() {
    print("Enter an integer: ")
    val enteredString = readLine()!!
    println("You have entered this: $enteredString")
    println("Result: " + factorial(enteredString.toInt()))
}

fun factorial(num: Int): Long {
    var result = 1L
    for (i in 2..num) result *= i
    return result
}