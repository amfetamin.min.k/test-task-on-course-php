import java.io.FileWriter
import kotlin.random.Random

fun main (args: Array<String>) {
    val randomValues = List(100) {
        Random.nextInt(0, 100)
    }
    writeToFile(randomValues.joinToString ( separator = "," ))
}

fun writeToFile(string: String) {
    var file = FileWriter("../numbers.csv")
    file.write(string)
    file.close()
}